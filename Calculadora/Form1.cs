﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        bool cliqueiBotoesOperacao = false;
        double numero1 = 0;
        double calculo = 0;
        bool mostrarCalculo = false;
        string qualOperacao = "";

        public void lerNumero(int numero)
        {
            if (textBox1.Text == "0")
            {
                textBox1.Text = "";
            }

            if (!cliqueiBotoesOperacao)
            {
                textBox1.Text += numero.ToString();
            }
            else
            {
                textBox1.Text = numero.ToString();
            }

            cliqueiBotoesOperacao = false;
        }

        public void botoesOperacao(string operacao)
        {
            cliqueiBotoesOperacao = true;
            if (mostrarCalculo)
            {
                calcularEMostrar();
            }
            numero1 = double.Parse(textBox1.Text);
            this.qualOperacao = operacao;
            mostrarCalculo = true;
        } 
        public void calcularEMostrar()
        {
            switch (qualOperacao) {

                default:
                case "+":
                    calculo = numero1 + Convert.ToDouble(textBox1.Text);
                    break;
                case "-":
                    calculo = numero1 - Convert.ToDouble(textBox1.Text);
                    break;
                case "/":
                    calculo = numero1 / Convert.ToDouble(textBox1.Text);
                    break;
                case "*":
                    calculo = numero1 * Convert.ToDouble(textBox1.Text);
                    break;
            }

            textBox1.Text = calculo.ToString();

        }

        public Form1()
        {
            InitializeComponent();
        }

        //botao backspace
        private void button1_Click(object sender, EventArgs e)
        {
            int tamanho = textBox1.Text.Length;
            if (tamanho > 0)
            {
                textBox1.Text = textBox1.Text.Substring(0, tamanho - 1);
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            lerNumero(0);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            lerNumero(1);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            lerNumero(2);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            lerNumero(3);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            lerNumero(4);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            lerNumero(5);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            lerNumero(6);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            lerNumero(7);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            lerNumero(8);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            lerNumero(9);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            botoesOperacao("/");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            botoesOperacao("*");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            botoesOperacao("-");
        }

        private void button16_Click(object sender, EventArgs e)
        {
            botoesOperacao("+");
        }

        //botao ce
        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "0";
        }

        //botao ponto
        private void button17_Click(object sender, EventArgs e)
        {
            textBox1.Text += ".";
        }
        //botao =
        private void button15_Click(object sender, EventArgs e)
        {
            calcularEMostrar();
            mostrarCalculo = false;
        }
    }
}
